package com.example.gpstracking;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AndroidGPSTrackingActivity extends Activity {

    Button btnShowLocation, btnSendSms;
    TextView myLocation;
    String result, mobileNumber, message;

    // GPSTracker class
    GPSTracker gps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnShowLocation = (Button) findViewById(R.id.btnShowLocation);
  //      btnSendSms = (Button) findViewById(R.id.sendsms);
        myLocation = (TextView) findViewById(R.id.mylocation);

        // show location button click event
        btnShowLocation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // create class object
                gps = new GPSTracker(AndroidGPSTrackingActivity.this);

                // check if GPS enabled
                if (gps.canGetLocation()) {

                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

//                    // \n is for new line
                  result = "My Current Location is - \nLatitude: " + latitude + "\nLongitude: " + longitude;
                  //  result = "My Current Location is-Latitude: " + latitude + " Longitude: " + longitude;
                    myLocation.setText(result);
                  //  Toast.makeText(getApplicationContext(), "" + result, Toast.LENGTH_LONG).show();
                } else {
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gps.showSettingsAlert();
                }

            }
        });

        btnSendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                mobileNumber = "+8801711084352";
              //  mobileNumber = "+8801945882352";
                message = result;
                if (!mobileNumber.equals("") && !message.equals("")) {
                    sendSMSMessage(mobileNumber, message);
                }
            }
        });
    }

    protected void sendSMSMessage(String number, String message) {

        Toast.makeText(getApplicationContext(), "SMS Sending...", Toast.LENGTH_SHORT).show();
        try {
            SmsManager smsManager = SmsManager.getDefault();

            Log.e("Message", message);

            if (message != null) {
                smsManager.sendTextMessage(number, null, message, null, null);
                Toast.makeText(getApplicationContext(), "SMS Send", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Can't send SMS", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            Log.e("Error", " " + e.toString());
        }

    }

}